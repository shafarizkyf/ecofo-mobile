import React from 'react';
import {
  View,
  Text,
  TextInput,
  Picker,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import mystyles from '../styles/mystyles';

const Ticket = (props) => {
  return (
    <View>
      <View style={ mystyles.row }>
        <View style={ mystyles.col }>
          <Text style={ mystyles.labelText }>Tiket</Text>
          <View style={{ borderColor:'grey', borderWidth: StyleSheet.hairlineWidth, borderRadius:3, marginBottom:10 }}>
            <Picker 
              onValueChange={ (val, index) => { 
                this.setState({
                  harga: props.tiketList[index].harga, 
                  id_tiket: val,
                  nama_tiket: props.tiketList[index].jenis_tiket,
                  total: this.state.jumlah * props.tiketList[index].harga
                }); 
              }}
              selectedValue={ props.tiketSelected }>
              { props.tiketList.map( obj => <Picker.Item label={ obj.jenis_tiket } key={obj.id} value={obj.id} />) }
            </Picker>
          </View>
        </View>
      </View>


      <View style={ mystyles.row }>
        <View style={ mystyles.col }>
          <Text style={ mystyles.labelText }>Jumlah</Text>
          <TextInput 
            style={[ mystyles.textInput, mystyles.textCenter ]}
            value={ props.jumlah }
            onChangeText={ (jumlah) => { this.updatePrice(jumlah) } }
            keyboardType='numeric'
            placeholder="Jumlah" />
        </View>

        <View style={ mystyles.col }></View>
        
        <View style={ mystyles.col }>
          <Text style={ mystyles.labelText }>Asal</Text>
          <TouchableOpacity onPress={ () => { this.setState({asal:'wni'}) } }>
            <Text style={ props.asal == 'wni' ? mystyles.labelFill : mystyles.labelOutline } >WNI</Text>
          </TouchableOpacity>
        </View>

        <View style={ mystyles.col }>
          <Text style={ mystyles.labelText }>{ ' ' }</Text>
          <TouchableOpacity onPress={ () => { this.setState({asal:'wna'}) } }>
            <Text style={ props.asal == 'wna' ? mystyles.labelFill : mystyles.labelOutline } >WNA</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default Ticket;