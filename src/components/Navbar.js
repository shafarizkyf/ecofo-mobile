import React from 'react';
import {
  View, 
  Text, 
  Image,
  TouchableOpacity, 
  StyleSheet,
  AsyncStorage
} from 'react-native';
import colors from '../config/colors';
import { withNavigation } from 'react-navigation';

const navbar = (props) => {

  return (
    <View style={{ height:50, backgroundColor:colors.green }}>
      <View style={ styles.navRow }>
        <TouchableOpacity onPress={props.openDrawer} activeOpacity={0.5}>
          <Image source={ require('../../assets/icon/menu.png') } style={ styles.iconMenu } />
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1}>
          <Text style={{ fontWeight:'bold', color:'white' }}>ECOFO</Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1}>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default navbar;

const styles = StyleSheet.create({
  iconMenu:{
    width:20,
    height:20,
  },
  navRow:{
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  counter:{
    position:'absolute',
    width:15,
    height:15,
    backgroundColor:'pink',
    fontSize: 8,
    borderRadius:8,
    textAlign:'center',
    textAlignVertical: 'center',
    color: 'white',
    backgroundColor: colors.green2,
    fontWeight: 'bold'
  }
});