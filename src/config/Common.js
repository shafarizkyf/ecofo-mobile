export default class Common {
  moneyFormat = (amount) => {
    let formatter = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0,
      currencyDisplay: 'name'
    });
    let formated = formatter.format(amount);
    return formated.replace('RP ', '');
  } 

}
