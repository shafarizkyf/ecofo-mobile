import React from 'react';
import {
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
  AsyncStorage,
} from 'react-native';
import { createSwitchNavigator, createDrawerNavigator, DrawerItems } from 'react-navigation';
import { Login, Transaksi, History } from '../pages';
import mystyles from '../styles/mystyles';

const _logout = async (props) => {
  await AsyncStorage.clear();
  props.navigation.navigate('login');
}

const CustomDrawerComponent = (props) => {
  return (
    <View style={[mystyles.page]}>
      <Image source={ require('../../assets/banner.jpg') } style={{width:400, height:200, alignSelf:'center'}} />
      <ScrollView>
        <CustomDrawerItems {...props} />
      </ScrollView>
    </View>
  )
};

const CustomDrawerItems = (props) => {
  return (
    <View style={[mystyles.page]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[mystyles.mh20, mystyles.mt20]}>
          <TouchableOpacity onPress={ () => { props.navigation.navigate('transaksi') } }>
            <Text style={[styles.item]}>Transaksi</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => { props.navigation.navigate('history') } }>
            <Text style={[styles.item]}>Riwayat Transaksi</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => { _logout(props) } }>
            <Text style={[styles.item]}>Logout</Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    </View>
  );
}

const Drawer = createDrawerNavigator({
  transaksi: {
    screen: Transaksi,
    navigationOptions: {
      title: 'Transaksi'
    }      
  },
  history: {
    screen: History,
    navigationOptions: {
      title: 'Riwayat Transaksi'
    }      
  },
}, {
  contentComponent: CustomDrawerComponent
})

const Switch = createSwitchNavigator({
  login: Login,
  main: Drawer
});

const styles = StyleSheet.create({
  item:{
    fontSize: 14,
    fontWeight: 'bold',
    color: '#0f9991',
    paddingVertical: 10,
    marginBottom: 5,
  } 
});

export default Switch;