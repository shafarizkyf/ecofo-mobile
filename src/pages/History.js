import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  ScrollView,
  AsyncStorage
} from 'react-native';
import SQLite from 'react-native-sqlite-storage';
import Spinner from 'react-native-spinkit';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import Navbar from '../components/Navbar';
import moment from 'moment';

const db = SQLite.openDatabase({ name:'ecofo.db', createFromLocation: '~ecofo.db' });
SQLite.enablePromise(true);

export default class History extends Component{
  
  state = {
    spinner: false,
    transaksi: []
  }

  async componentDidMount(){
    let id_user = await AsyncStorage.getItem('id_user');
    if(id_user){
      this.dataTransaksi(id_user);
    }
  }

  dataTransaksi(id_user){
    var transaksi = [];
    db._55.executeSql(`select * from transaksi where id_user=${id_user} order by id desc`).then(results => {
      for(let i=0; i<results[0].rows.length; i++){
        let row = results[0].rows.item(i);
        transaksi.push(row);
        console.log(row);
      }
      this.setState({transaksi})
    });
  }

  render(){

    function Card(props){
      return (
        <View style={[styles.card, mystyles.mt10]}>
          <View style={[mystyles.row, mystyles.spaceBetween]}>
            <View style={[mystyles.column]}>
              <Text style={{fontSize:12}}>{ props.tanggal }</Text>
              <Text style={[mystyles.bold]}>{ props.tiket }</Text>
              <Text style={[styles.synced, styles.pullLeft, props.isSync ? mystyles.bgBlue : mystyles.bgRed]}>
                {props.isSync ? 'Synced' : 'Need to sync'}
              </Text>
            </View>
            <Text style={styles.counter}>{ props.jumlah }</Text>
          </View>
        </View>
      );
    }

    return(
      <View style={ mystyles.wrapper }>
        <View style={[mystyles.spinnerWrapper, !this.state.spinner ? { display:'none', position:'relative' } : {} ]}>
          <Spinner isVisible={ this.state.spinner } type="Bounce" color={ '#fff' } size={150} style={{ justifyContent:'center', flex:1  }} />
          <Text style={{ color:'white', marginBottom:20 }}>{ this.spinnerText }</Text>
        </View>

        <ImageBackground source={ require('../../assets/bg3.jpg') } style={mystyles.bg} />
        <View style={{ width:'100%', height:'100%', backgroundColor:'rgba(0,0,0,0.2)', position:'absolute' }}></View>


        <Navbar openDrawer={()=>this.props.navigation.openDrawer()} />
        <ScrollView>
          <View>
            <View style={[mystyles.mh20]}>

              {
                this.state.transaksi.map(obj => {
                  let date = moment(obj.created_at, 'YYYY-MM-DD HH:mm:ss').add(7, 'h').format('DD-MM-YYYY HH:mm:ss');
                  return <Card tanggal={ date } tiket={ `${obj.nama_tiket} - ${obj.asal.toUpperCase()}`  } key={obj.id} jumlah={ obj.jumlah } isSync={obj.sync} />
                })
              }
            
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  wrapper:{
    flex:1,
    backgroundColor: colors.green2,
  },
  card:{
    backgroundColor:'white',
    padding: 10,
    borderRadius: 3,
  },
  counter:{
    width:40,
    height:40,
    backgroundColor: colors.blue,
    color:'white',
    padding: 5,
    borderRadius: 25,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 14,
    fontWeight: 'bold'
  },
  synced:{
    marginTop: 5,
    paddingHorizontal: 5,
    fontSize: 12,
    color: 'white',
    borderRadius: 5,
    textAlign: 'center'
  },
  pullLeft:{
    alignSelf:'flex-start',
    alignItems:'flex-start'
  }
})