import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Alert,
  AsyncStorage
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';
import axios from 'axios';
import qs from 'qs';
import api from '../config/api';

export default class Login extends Component{
  
  state = {
    username: null,
    password: null,
  }

  async componentDidMount(){
    let isLoggedIn = await AsyncStorage.getItem('id_user');
    if(isLoggedIn){
      this.navigateToMainPage();
    }
  }

  login(){
    
    if(this.state.username && this.state.password){
      axios.post(api.baseUrl + api.login, qs.stringify(this.state)).then(response => {
        let data = response.data;
        console.log('user -->', data);

        if(!data.login){
          Alert.alert('Gagal Login', data.msg);
        }else{
          AsyncStorage.setItem('id_user', data.user.id.toString());
          AsyncStorage.setItem('nama_user', data.user.nama);
          AsyncStorage.setItem('tiket', JSON.stringify(data.user.tiket));
          this.navigateToMainPage();
        }
      }).catch(error => {
        console.log(error);
      });
    }
  }

  navigateToMainPage(){
    this.props.navigation.navigate('main');
  }

  render(){
    return(
      <View style={ styles.wrapper }>
        <ImageBackground source={ require('../../assets/bg.jpg') } style={mystyles.bg} />
        <View>
          <Image source={ require('../../assets/logo.png') } style={ styles.logo } />
          <Text style={[ styles.logoText, mystyles.mb30]}>ECOFO</Text>
        </View>
        <View style={[ mystyles.formWrapper, {marginHorizontal:40}]}>
          <TextInput 
            style={ styles.textInput }
            placeholder="Username" 
            onChangeText={ (username) => { this.setState({ username }) } }/>

          <TextInput 
            style={ styles.textInput }
            placeholder="Password"
            secureTextEntry={true} 
            onChangeText={ (password) => { this.setState({ password }) } }/>

          <TouchableOpacity onPress={ () => { this.login() } }>
            <Text style={[mystyles.btn, mystyles.mt30]}>Login</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo:{
    width:100,
    height:100,
    alignSelf: 'center',
    marginBottom: 5
  },
  logoText:{
    color:'#212121',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10
  },
  wrapper: {
    flex:1,
    justifyContent: 'center',
  },
  textInput: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'grey',
    fontSize: 14
  }
});