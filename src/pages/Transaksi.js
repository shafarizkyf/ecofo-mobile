import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Picker,
  StyleSheet,
  Alert,
  AsyncStorage,
} from 'react-native';
import NetworkState from 'react-native-network-state'
import SQLite from 'react-native-sqlite-storage';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import Navbar from '../components/Navbar';
import axios from 'axios';
import api from '../config/api';
import qs from 'qs';
import moment from 'moment';
import Spinner from 'react-native-spinkit';
import Ticket from '../components/Ticket';

const db = SQLite.openDatabase({ name:'ecofo.db', createFromLocation: '~ecofo.db' });
SQLite.enablePromise(true);

export default class Transaksi extends Component {

  state = {
    jumlah: null,
    asal: 'wni',
    total: 0,
    spinner: false,
    tiketList: []
  }

  async componentDidMount(){
    
    //this.sync(); <--- if this turn on, the sync will have 2 transaction when move from other page && network state changed

    let id_user = await AsyncStorage.getItem('id_user');
    let nama_user = await AsyncStorage.getItem('nama_user');
    let tiketList = await AsyncStorage.getItem('tiket');

    if(id_user && nama_user){
      tiketList = JSON.parse(tiketList);
      console.log('tiket -->', tiketList);

      this.setState({ 
        tiketList, 
        id_user,
        nama_user,
        spinner: false, 
        harga: tiketList[0].harga,
        id_tiket: tiketList[0].id,
        nama_tiket: tiketList[0].jenis_tiket,
      });

    }else{
      await AsyncStorage.clear();
      this.props.navigation.navigate('main');
    }
    
  }

  print(){
    if(this.state.asal != '' && this.state.jumlah != 0){
      this.state.isConnected ?  this.onlineMode() : this.offlineMode();
    }
  }


  updatePrice(jumlah){
    var updatePrice = true;
    if(jumlah > 10){
      Alert.alert('Konfirmasi Transaksi', `Anda akan membuat transaksi tiket sejumlah ${jumlah}`, [
        {text:'Tidak', onPress: () => { this.setState({jumlah:null, total:'0'}) } },
        {text:'Ya' },
      ]);
    }

    if(updatePrice){
      this.setState({ jumlah, total: this.state.harga * jumlah });
    }else{
      this.setState({ jumlah:null, total: 0 });
    }
  }

  
  offlineMode(){
    this.saveTransaction();
  }

  onlineMode(){
    if(this.state.jumlah){
      this.setState({ spinner:true, spinnerText:'Menyimpan data' });
      axios.post(api.baseUrl + api.transaksi, qs.stringify(this.state))
      .then(response => {
        let data = response.data;
        if(data.success){
          this.saveTransaction();
          this.setState({ spinner:false });
          Alert.alert('Transaksi berhasil', 'Data transaksi berhasil terkirim ke server');
        }
      })
      .catch(error => {
        console.log('error transaksi -->', error);
      });
    }
  }

  saveTransaction(){
    db.transaction((tx) => {
      tx.executeSql('INSERT INTO transaksi (id_user, nama_user, id_tiket, nama_tiket, jumlah, asal, created_at, sync) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', 
      [
        this.state.id_user,
        this.state.nama_user,
        this.state.id_tiket,
        this.state.nama_tiket,
        this.state.jumlah,
        this.state.asal,
        moment().utc().format('YYYY-MM-DD HH:mm:ss'),
        this.state.isConnected,
      ], (tx, results) => {
        if(results.rowsAffected){
          this.setState({jumlah:null, total:0});
        }

        if(!this.state.isConnected){
          Alert.alert('Transaksi berhasil', 'Data akan di sinkronasi ketika memiliki koneksi internet');
        }
      });
    });
  }

  sync(){
    this.setState({ spinner:true, spinnerText:'Sinkronasi data dengan server' });
    db.executeSql('SELECT * FROM transaksi WHERE sync=?', [0]).then(results => {
      for(let i=0; i<results[0].rows.length; i++){
        let row = results[0].rows.item(i);
        if(this.state.isConnected){
          axios.post(api.baseUrl + api.transaksi, qs.stringify(row)).then(response => {
            db.executeSql('UPDATE transaksi SET sync=? WHERE id=?', [1, row.id]).then(results => {
              let result = results[0].rowsAffected;
              console.log('server response -->', response);
              console.log('local updated -->', result);
            }) 
          });
        }
      }
      this.setState({ spinner:false });
    });
  }

  async onConnected(){
    this.setState({ isConnected: true });
    await this.sync();
  }

  onDisconnected(){
    this.setState({ isConnected: false });
    console.log('no internet');
  }

  render(){
    return(
      <View style={ styles.wrapper }>
        <ImageBackground source={ require('../../assets/bg3.jpg') } style={mystyles.bg} />
        <View style={[styles.spinnerWrapper, !this.state.spinner ? { display:'none', position:'relative' } : {} ]}>
          <Spinner isVisible={ this.state.spinner } type="Bounce" color={ '#fff' } size={150} style={{ justifyContent:'center', flex:1  }} />
          <Text style={{ color:'white', marginBottom:20 }}>{ this.spinnerText }</Text>
        </View>
        <Navbar openDrawer={()=>this.props.navigation.openDrawer()} />
        <View>
          <NetworkState
            onConnected={ () => { this.onConnected() } } 
            onDisconnected={ () => { this.onDisconnected() } }
            />
        </View>
        <View style={[ mystyles.formWrapper, mystyles.mt30]}>

          <View style={ mystyles.row }>
            <View style={ mystyles.col }>
              <Text style={ mystyles.labelText }>Printer</Text>
              <View style={{ borderColor:'grey', borderWidth: StyleSheet.hairlineWidth, borderRadius:3, marginBottom:10 }}>
                <Picker>
                  <Picker.Item label="Pilih Printer" value="" />
                </Picker>
              </View>
            </View>
          </View>

          <View style={ mystyles.row }>
            <View style={ mystyles.col }>
              <Text style={ styles.labelText }>Tiket</Text>
              <View style={{ borderColor:'grey', borderWidth: StyleSheet.hairlineWidth, borderRadius:3, marginBottom:10 }}>
                <Picker 
                  onValueChange={ (val, index) => { 
                    this.setState({
                      harga: this.state.tiketList[index].harga, 
                      id_tiket: val,
                      nama_tiket: this.state.tiketList[index].jenis_tiket,
                      total: this.state.jumlah * this.state.tiketList[index].harga
                    }); 
                  }}
                  selectedValue={ this.state.id_tiket }>
                  { this.state.tiketList.map( obj => <Picker.Item label={ obj.jenis_tiket } key={obj.id} value={obj.id} />) }
                </Picker>
              </View>
            </View>
          </View>


          <View style={ mystyles.row }>
            <View style={ mystyles.col }>
              <Text style={ styles.labelText }>Jumlah</Text>
              <TextInput 
                style={[ styles.textInput, mystyles.textCenter ]}
                value={ this.state.jumlah }
                onChangeText={ (jumlah) => { this.updatePrice(jumlah) } }
                keyboardType='numeric'
                placeholder="Jumlah" />
            </View>

            <View style={ mystyles.col }></View>
            
            <View style={ mystyles.col }>
              <Text style={ styles.labelText }>Asal</Text>
              <TouchableOpacity onPress={ () => { this.setState({asal:'wni'}) } }>
                <Text style={ this.state.asal == 'wni' ? styles.labelFill : styles.labelOutline } >WNI</Text>
              </TouchableOpacity>
            </View>

            <View style={ mystyles.col }>
              <Text style={ styles.labelText }>{ ' ' }</Text>
              <TouchableOpacity onPress={ () => { this.setState({asal:'wna'}) } }>
                <Text style={ this.state.asal == 'wna' ? styles.labelFill : styles.labelOutline } >WNA</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[mystyles.row, mystyles.mt10]}>
            <View style={ mystyles.col }>
              <Text style={ mystyles.labelText }>Total</Text>
              <Text style={ styles.price }>Rp { this.state.total }</Text>
            </View>
          </View>

          <View style={[mystyles.row, mystyles.mt10]}>
            <View style={ mystyles.col }>
              <TouchableOpacity onPress={ () => { this.print() } }>
                <Text style={[mystyles.btn]}>Cetak Tiket</Text>
              </TouchableOpacity>
            </View>
          </View>

        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  wrapper:{
    flex:1,
  },
  spinnerWrapper:{
    position:'absolute', 
    width:'100%', 
    height:'100%', 
    backgroundColor: 'rgba(42, 147, 239, 1)',
    zIndex:2, 
    flex:1, 
    alignItems:'center', 
    alignContent:'center',
  },
  textInput:{
    fontSize:14,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    borderRadius: 3
  },
  labelOutline:{
    color: colors.blue, 
    width:'100%', 
    height: 50,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderColor: colors.blue, 
    borderRadius: 3,
    borderWidth: StyleSheet.hairlineWidth
  },
  labelFill:{
    color: 'white', 
    width:'100%', 
    height: 50,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: colors.blue,
    borderRadius: 3
  },
  labelText:{
    fontSize:12,
    fontWeight: 'bold',
    color: 'grey',
    marginBottom:3
  },
  price:{
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 10,
    backgroundColor: '#F3F3F3'
  }
});