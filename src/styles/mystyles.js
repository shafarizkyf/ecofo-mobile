import { StyleSheet } from 'react-native';
import colors from '../config/colors';

export default StyleSheet.create({
  mh20:{
    marginHorizontal:20
  },
  mt10:{
    marginTop:10
  },
  mt20:{
    marginTop:20
  },
  mt30:{
    marginTop:30
  },
  mt40:{
    marginTop:40
  },
  mt50:{
    marginTop:50
  },
  mb30:{
    marginBottom:30
  },
  bgRed:{
    backgroundColor: colors.red
  },
  bgBlue:{
    backgroundColor: colors.blue
  },
  spaceBetween:{
    justifyContent: 'space-between'
  },
  btn:{
    backgroundColor: colors.blue,
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
    paddingVertical: 15,
    marginTop: 10,
    borderRadius: 3
  },
  formWrapper:{
    backgroundColor: 'white',
    marginHorizontal: 20,
    padding:20,
    borderRadius: 5,
  },
  col:{
    flex:1,
    marginHorizontal: 5
  },
  row: {
    flexDirection: 'row',
    width:'100%',
  },
  column: {
    flexDirection: 'column',
  },
  bold: {
    fontWeight: 'bold',
  },
  textCenter:{
    textAlign: 'center'
  },
  wrapper:{
    flex:1,
    backgroundColor: colors.green2,
  },
  spinnerWrapper:{
    position:'absolute', 
    width:'100%', 
    height:'100%', 
    backgroundColor: 'rgba(42, 147, 239, 1)',
    zIndex:2, 
    flex:1, 
    alignItems:'center', 
    alignContent:'center',
  },
  bg:{
    width:'100%',
    height:'100%',
    position: 'absolute',
    top:0,
    bottom:0,
    left:0,
    right:0,
  },
  textInput:{
    fontSize:14,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    borderRadius: 3
  },
  labelText:{
    fontSize:12,
    fontWeight: 'bold',
    color: 'grey',
    marginBottom:3
  },
  labelOutline:{
    color: colors.blue, 
    width:'100%', 
    height: 50,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderColor: colors.blue, 
    borderRadius: 3,
    borderWidth: StyleSheet.hairlineWidth
  },
  labelFill:{
    color: 'white', 
    width:'100%', 
    height: 50,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: colors.blue,
    borderRadius: 3
  },
})