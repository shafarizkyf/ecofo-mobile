import React, { Component } from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
} from 'react-native';
import MyNavigation from './src/navigation/MyNavigation';
// import BleManager from 'react-native-ble-manager';
// import { BLEPrinter } from 'react-native-printer';

export default class App extends Component {
  
  // state = {
  //   printers:[],
  //   blEnabled: false
  // }

  // async componentDidMount(){
  //   BleManager.start();
  //   BleManager.enableBluetooth().then(() => { 
  //     BLEPrinter.init().then(()=> {
  //       this.getDeviceList();
  //     });
  //   });    
  // }

  // async getDeviceList(){
  //   BLEPrinter.getDeviceList()
  //   .then(printers => {
  //     console.log('printers -->', printers);
  //     this.setState({printers});
  //     this.createBond();
  //   });
  // }

  // async createBond(){
  //   if(this.state.printers.length){
  //     BleManager.createBond(this.state.printers[0].inner_mac_address)
  //     .then(() => {
  //       console.log('pairing success');
  //       BleManager.connect(this.state.printers[0].inner_mac_address)
  //       .then(() => { this.print() })
  //     })
  //   }
  // }

  // async print(){
  //   BLEPrinter.connectPrinter(this.state.printers[0].inner_mac_address)
  //   .then(printer => {
  //     console.log('printing...');
  //     BLEPrinter.printText("<QRI>12113\n");
  //     BLEPrinter.printText("<C>Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World</C>\n");
  //     BLEPrinter.printText("<QR>"+ require('./assets/qr.png') +"</QR>\n");
  //   })
  // }


  render() {
    return (
      <View style={styles.container}>
        <MyNavigation />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
